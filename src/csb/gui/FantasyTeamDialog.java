package csb.gui;

import csb.data.Lecture;
import csb.data.Player;
import csb.data.FantasyTeam;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Victor Cheng
 */
public class FantasyTeamDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    FantasyTeam fantasyTeam;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    
    Label nameLabel;
    Label ownerLabel;
    TextField nameTextField;
    TextField ownerTextField;
    
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    
    public static final String NAME_PROMPT = "Name: ";
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String PRO_TEAM_PROMPT = "Pro Team: ";
    
    public static final String SESSIONS_PROMPT = "Number of Sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    
    public static final String FANTASY_TEAM_HEADING = "Fantasy Team Details";
    
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";
    
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public FantasyTeamDialog(Stage primaryStage) {
        // FIRST MAKE OUR LECTURE AND INITIALIZE
        // IT WITH DEFAULT VALUES
        fantasyTeam = new FantasyTeam();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(FANTASY_TEAM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC 
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            fantasyTeam.setName(newValue);
        });
        
        // AND THE NUMBER OF SESSIONS
        ownerLabel = new Label(OWNER_PROMPT);
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ownerTextField = new TextField();
        ownerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            fantasyTeam.setOwner(newValue);
        });



        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            FantasyTeamDialog.this.selection = sourceButton.getText();
            FantasyTeamDialog.this.hide();
        };
        
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(nameLabel,        0, 1, 1, 1);
        gridPane.add(nameTextField,    1, 1, 1, 1);
        gridPane.add(ownerLabel,     0, 2, 1, 1);
        gridPane.add(ownerTextField,  1, 2, 1, 1);
//        gridPane.add(proTeamLabel,     0, 3, 1, 1);
//        gridPane.add(proTeamComboBox,  1, 3, 1, 1);        
//        gridPane.add(posCCheckBox, 0, 4 , 1, 1);
//        gridPane.add(posOneBCheckBox, 1, 4 , 1, 1);
//        gridPane.add(posTwoBCheckBox, 2, 4 , 1, 1);
//        gridPane.add(posThreeBCheckBox, 3, 4 , 1, 1);
//        gridPane.add(posScCheckBox, 4, 4 , 1, 1);
        
        gridPane.add(completeButton,    0, 4, 1, 1);
        gridPane.add(cancelButton,      1, 4, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public FantasyTeam getFantasyTeam() { 
        return fantasyTeam;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public FantasyTeam showAddFantasyTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        fantasyTeam = new FantasyTeam();
        
        // LOAD THE UI STUFF
        nameTextField.setText(fantasyTeam.getName());
        ownerTextField.setText(fantasyTeam.getOwner());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return fantasyTeam;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        nameTextField.setText(fantasyTeam.getName());
        
        //int sessionsIndex = lecture.getSessions()-1;
        //sessionsComboBox.getSelectionModel().select(sessionsIndex);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditFantasyTeamDialog(FantasyTeam teamToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_FANTASY_TEAM_TITLE);
        
        // LOAD THE LECTURE INTO OUR LOCAL OBJECT
        fantasyTeam.setName(teamToEdit.getName());
        fantasyTeam.setOwner(teamToEdit.getOwner());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
}