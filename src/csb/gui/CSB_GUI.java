package csb.gui;

import static csb.CSB_StartupConstants.*;
import csb.CSB_PropertyType;
import csb.controller.CourseEditController;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.CourseDataView;
import csb.data.CoursePage;
import csb.controller.FileController;
import csb.controller.ScheduleEditController;
import csb.data.Assignment;
import csb.data.Instructor;
import csb.data.Lecture;
import csb.data.ScheduleItem;
import csb.data.Semester;
import csb.data.Subject;
import csb.data.FantasyTeam;
import csb.data.Player;
import csb.data.Pitcher;
import csb.data.Hitter;
import csb.data.PlayerList;
import csb.file.CourseFileManager;
import csb.file.CourseSiteExporter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import properties_manager.PropertiesManager;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Course and exporting it to a
 * site.
 *
 * @author Richard McKenna
 */
public class CSB_GUI implements CourseDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    String JSON_PITCHER = "Pitchers";
    String JSON_HITTER = "Hitters";
    
    String JSON_TEAM = "TEAM";
    String JSON_LAST = "LAST_NAME";
    String JSON_FIRST = "FIRST_NAME";
    
    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV = "SV";
    String JSON_H = "H";
    String JSON_BB = "BB";
    String JSON_K = "K";
    
    String JSON_R = "R";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
//    String JSON_BA = "BA";
    String JSON_QP = "QP";
    
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    
    
    String JSON_EXT = ".json";
    String SLASH = "/";

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    CourseDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    CourseFileManager courseFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    CourseSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    CourseEditController courseController;
    
    // THIS HANDLES REQUESTS TO ADD OR EDIT SCHEDULE STUFF
    ScheduleEditController scheduleController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;
    
    Scene fantasyTeamsScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane csbPane;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newCourseButton;
    Button loadCourseButton;
    Button saveCourseButton;
    Button exportSiteButton;
    Button exitButton;
    
    // THIS IS THE BOTTOM TOOLBAR AND ITS CONTROLS
    FlowPane draftToolbarPane;
    Button fantasyTeamsButton;
    Button playersButton;
    Button standingsButton;
    Button draftButton;
    Button mlbButton;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;
    
    // WE'LL PUT THE WORKSPACE INSIDE A SCROLL PANE
    ScrollPane workspaceScrollPane;

    // WE'LL PUT THIS IN THE TOP OF THE WORKSPACE, IT WILL
    // HOLD TWO OTHER PANES FULL OF CONTROLS AS WELL AS A LABEL
    VBox topWorkspacePane;
    Label courseHeadingLabel;
    SplitPane topWorkspaceSplitPane;
    
    Pane topFantasyTeamsPane;
    Label fantasyTeamsHeadingLabel;
    
    VBox topPlayersPane;
    Label playersHeadingLabel;
    
    VBox topFantasyStandingsPane;
    Label fantasyStandingsHeadingLabel;
    
    VBox topDraftPane;
    Label draftHeadingLabel;
    
    VBox topMlbTeamsPane;
    Label mlbTeamsHeadingLabel;
    ComboBox mlbTeamComboBox;
    Label mlbTeamsComboBoxLabel;
    HBox mlbTeamToolbar;

    // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
    GridPane courseInfoPane;
    Label courseInfoLabel;
    Label courseSubjectLabel;
    ComboBox courseSubjectComboBox;
    Label courseNumberLabel;
    TextField courseNumberTextField;
    Label courseSemesterLabel;
    ComboBox courseSemesterComboBox;
    Label courseYearLabel;
    ComboBox courseYearComboBox;
    Label courseTitleLabel;
    TextField courseTitleTextField;
    Label instructorNameLabel;
    TextField instructorNameTextField;
    Label instructorURLLabel;
    TextField instructorURLTextField;
    
    Label searchPlayerLabel;
    TextField searchPlayerTextField;
    
    GridPane searchPlayerPane;
    
    HBox searchPlayerToolbar;
    
    
    Label draftNameLabel;
    TextField draftNameTextField;
    GridPane FantasyTeamNamePane;
    HBox FantasyTeamNameToolbar;
    
    Label fantasyTeamNameLabel;
    ComboBox fantasyTeamNameComboBox;
    
    GridPane FantasyTeamsInfoPane;
    // THESE ARE THE CONTROLS FOR SELECTING WHICH PAGES THE SCHEDULE
    // PAGE WILL HAVE TO LINK TO
    VBox pagesSelectionPane;
    Label pagesSelectionLabel;
    CheckBox indexPageCheckBox;
    CheckBox syllabusPageCheckBox;
    CheckBox schedulePageCheckBox;
    CheckBox hwsPageCheckBox;
    CheckBox projectsPageCheckBox;

    // SCHEDULE CONTROLS
    VBox schedulePane;
    VBox scheduleInfoPane;
    Label scheduleInfoHeadingLabel;
    SplitPane splitScheduleInfoPane;
    
    VBox fantasyTeamPane;

    VBox MLBTeamPane;
    
    // THESE GUYS GO IN THE LEFT HALF OF THE splitScheduleInfoPane
    GridPane dateBoundariesPane;
    Label dateBoundariesLabel;
    Label startDateLabel;
    DatePicker startDatePicker;
    Label endDateLabel;
    DatePicker endDatePicker;

    // THESE GUYS GO IN THE RIGHT HALF OF THE splitScheduleInfoPane
    VBox lectureDaySelectorPane;
    Label lectureDaySelectLabel;
    CheckBox mondayCheckBox;
    CheckBox tuesdayCheckBox;
    CheckBox wednesdayCheckBox;
    CheckBox thursdayCheckBox;
    CheckBox fridayCheckBox;
    
    ObservableList<Player> playersList = FXCollections.observableArrayList();
    
    ObservableList<Player> noEditPlayersList = FXCollections.observableArrayList();
    
    ObservableList<Player> radioPlayersList = FXCollections.observableArrayList();
    
    ObservableList<Player> fantasyTeamList = FXCollections.observableArrayList();
    
    ObservableList<String> fantasyTeamNames = FXCollections.observableArrayList();
    
    ObservableList<Player> mlbTeamList = FXCollections.observableArrayList();
    
    ObservableList<Player> tempList = FXCollections.observableArrayList();
    
    
    VBox playersBox;
    HBox playersToolbar;
    Button addPlayerButton;
    Button removePlayerButton;
    Button editPlayerButton;
    Label playersLabel;
    TableView<Player> playersTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn birthYearColumn;
    TableColumn runsWinsColumn;
    TableColumn homeRunsSavesColumn;
    TableColumn runsBattedInStrikeoutsColumn;
    TableColumn stolenBasesEarnedRunAverageColumn;
    TableColumn battingAverageWalksHitsInningsPitchedColumn;
    TableColumn estimatedValueColumn;
    TableColumn notesColumn;
    
    Button addFantasyTeamButton;
    Button removeFantasyTeamButton;
    Button editFantasyTeamButton;
    Label fantasyTeamLabel;
    Label startingLineupLabel;
    TableView<Player> fantasyTeamsTable;
    TableColumn ftPositionColumn;    
    TableColumn ftFirstNameColumn;
    TableColumn ftLastNameColumn;
    TableColumn ftProTeamColumn;
    TableColumn ftPositionsColumn;   
    TableColumn ftRunsWinsColumn;
    TableColumn ftHomeRunsSavesColumn;
    TableColumn ftRunsBattedInStrikeoutsColumn;
    TableColumn ftStolenBasesEarnedRunAverageColumn;
    TableColumn ftBattingAverageWalksHitsInningsPitchedColumn;
    TableColumn ftEstimatedValueColumn;
    TableColumn ftContractColumn;
    TableColumn ftSalaryColumn;
    Label taxiSquadLabel;
    TableView<Player> taxiSquadTable;
    TableColumn tsPositionColumn;    
    TableColumn tsFirstNameColumn;
    TableColumn tsLastNameColumn;
    TableColumn tsProTeamColumn;
    TableColumn tsPositionsColumn;   
    TableColumn tsRunsWinsColumn;
    TableColumn tsHomeRunsSavesColumn;
    TableColumn tsRunsBattedInStrikeoutsColumn;
    TableColumn tsStolenBasesEarnedRunAverageColumn;
    TableColumn tsBattingAverageWalksHitsInningsPitchedColumn;
    TableColumn tsEstimatedValueColumn;
    TableColumn tsContractColumn;
    TableColumn tsSalaryColumn;
    
    VBox fantasyStandingsPane;
    TableView<FantasyTeam> fantasyStandingsTable;
    TableColumn fsTeamNameColumn;
    TableColumn fsPlayersNeededColumn;
    TableColumn fsMoneyLeftColumn;
    TableColumn fsMoneyPerPlayerColumn;
    TableColumn fsRColumn;
    TableColumn fsHrColumn;
    TableColumn fsRbiColumn; 
    TableColumn fsSbColumn; 
    TableColumn fsBaColumn; 
    TableColumn fsWColumn; 
    TableColumn fsSvColumn; 
    TableColumn fsKColumn; 
    TableColumn fsEraColumn; 
    TableColumn fsWhipColumn;
    TableColumn fsTotalPointsColumn;
    
    TableView<Player> mlbTeamsTable;
    TableColumn mlbPositionColumn;    
    TableColumn mlbFirstNameColumn;
    TableColumn mlbLastNameColumn;
    
    VBox draftPane;
    HBox draftToolbar;
    Button draftStarButton;
    Button draftStartButton;
    Button draftPauseButton;
    TableView<Player> draftTable;
    TableColumn draftPickColumn;    
    TableColumn draftFirstNameColumn;
    TableColumn draftLastNameColumn;
    TableColumn draftTeamColumn;
    TableColumn draftContractColumn;
    TableColumn draftSalaryColumn;

    
    // AND TABLE COLUMNS
    static final String COL_FIRST = "First";
    static final String COL_LAST = "Last";
    static final String COL_TEAM = "Pro Team";
    static final String COL_POSITION = "Positions";
    static final String COL_BIRTHYEAR = "Year of Birth";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_ESTIMATED_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";

    static final String  COL_FT_POSITION = "Position";    
    static final String  COL_FT_FIRST = "First";
    static final String  COL_FT_LAST = "Last";
    static final String  COL_FT_TEAM = "Pro Team";
    static final String  COL_FT_POSITIONS = "Positions";   
    static final String  COL_FT_R_W = "R/W";
    static final String  COL_FT_HR_SV = "HR/SV";
    static final String  COL_FT_RBI_K = "RBI/K";
    static final String  COL_FT_SB_ERA = "SB/ERA";
    static final String  COL_FT_BA_WHIP = "BA/WHIP";
    static final String  COL_FT_ESTIMATED_VALUE = "Estimated Value";
    static final String  COL_FT_CONTRACT = "Contract";
    static final String  COL_FT_SALARY = "Salary";
    
    static final String  COL_FS_TEAM_NAME = "Team Name";
    static final String  COL_FS_PLAYERS_NEEDED = "Players Needed";
    static final String  COL_FS_MONEY_LEFT = "$ Left";
    static final String  COL_FS_MONEY_PER_PLAYER = "$ PP";
    static final String  COL_FS_R = "R";
    static final String  COL_FS_HR = "HR";
    static final String  COL_FS_RBI = "RBI";
    static final String  COL_FS_SB = "SB";
    static final String  COL_FS_BA = "BA";
    static final String  COL_FS_W = "W";
    static final String  COL_FS_SV = "SV";
    static final String  COL_FS_K = "K";
    static final String  COL_FS_ERA = "ERA";
    static final String  COL_FS_WHIP = "WHIP";
    static final String  COL_FS_TOTAL_POINTS = "Total Points";
    
    static final String  COL_D_PICK_NUMBER = "Pick #";
    static final String  COL_D_FIRST = "First";
    static final String  COL_D_LAST = "Last";
    static final String  COL_D_TEAM = "Team";
    static final String  COL_D_CONTRACT = "Contract";
    static final String  COL_D_SALARY = "Salary ($)";
    
    
    // THIS REGION IS FOR MANAGING SCHEDULE ITEMS OTHER THAN LECTURES AND HWS
    VBox scheduleItemsBox;
    HBox scheduleItemsToolbar;
    
    VBox fantasyTeamsBox;
    HBox fantasyTeamsToolbar;
       
    GridPane radioButtonsPane;
    RadioButton all, c, oneB, ci, threeB, twoB, mi, ss, of, u, p;
    HBox radioButtonsToolbar;
    
    ToggleGroup group;
    
    Button addScheduleItemButton;
    Button removeScheduleItemButton;
    
    Label scheduleItemsLabel;
    TableView<Player> scheduleItemsTable;
    TableColumn itemDescriptionsColumn;
    TableColumn itemDatesColumn;
    TableColumn linkColumn;
    
    // THIS REGION IS FOR MANAGING LECTURES
    VBox lecturesBox;
    HBox lecturesToolbar;
    Button addLectureButton;
    Button removeLectureButton;
    Button moveUpLectureButton;
    Button moveDownLectureButton;
    Label lecturesLabel;
    TableView<Lecture> lecturesTable;
    TableColumn lectureTopicsColumn;
    TableColumn lectureSessionsColumn;
    
    // THIS REGION IS FOR MANAGING HWs
//    VBox hwsBox;
//    HBox hwsToolbar;
//    Button addHWButton;
//    Button removeHWButton;
//    Label hwsLabel;
//    TableView<Assignment> hwsTable;
//    TableColumn hwNamesColumn;
//    TableColumn hwTopicsColumn;
//    TableColumn hwDatesColumn;
    
    // AND TABLE COLUMNS
    static final String COL_DESCRIPTION = "Description";
    static final String COL_DATE = "Date";
    static final String COL_LINK = "Link";
    static final String COL_TOPIC = "Topic";
    static final String COL_SESSIONS = "Number of Sessions";
    static final String COL_NAME = "Name";
    static final String COL_TOPICS = "Topics";
    
    // HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ProgressDialog progressDialog;
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.
     */
    public CSB_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The CourseDataManager used by this UI.
     */
    public CourseDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the course file manager.
     *
     * @return The CourseFileManager used by this UI.
     */
    public CourseFileManager getCourseFileManager() {
        return courseFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The CourseSiteExporter used by this UI.
     */
    public CourseSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(CourseDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * Mutator method for the course file manager.
     *
     * @param initCourseFileManager The CourseFileManager to be used by this UI.
     */
    public void setCourseFileManager(CourseFileManager initCourseFileManager) {
        courseFileManager = initCourseFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param initSiteExporter The CourseSiteExporter to be used by this UI.
     */
    public void setSiteExporter(CourseSiteExporter initSiteExporter) {
        siteExporter = initSiteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param subjects The list of subjects to choose from.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle, ArrayList<String> subjects) throws IOException {
        // INIT THE DIALOGS
        initDialogs();
        
        // INIT THE TOOLBAR
        initFileToolbar();
        
        initDraftToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace(subjects);

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing a Course.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(workspaceScrollPane);
            workspaceActivated = true;
        }
    }
    
    
    /**
     * This function takes all of the data out of the courseToReload 
     * argument and loads its values into the user interface controls.
     * 
     * @param courseToReload The Course whose data we'll load into the GUI.
     */
    @Override
    public void reloadCourse(Course courseToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        courseController.enable(false);

        // FIRST LOAD ALL THE BASIC COURSE INFO
        courseSubjectComboBox.setValue(courseToReload.getSubject());
        courseNumberTextField.setText("" + courseToReload.getNumber());
        courseSemesterComboBox.setValue(courseToReload.getSemester());
        courseYearComboBox.setValue(courseToReload.getYear());
        courseTitleTextField.setText(courseToReload.getTitle());
        instructorNameTextField.setText(courseToReload.getInstructor().getName());
        instructorURLTextField.setText(courseToReload.getInstructor().getHomepageURL());
        indexPageCheckBox.setSelected(courseToReload.hasCoursePage(CoursePage.INDEX));
        syllabusPageCheckBox.setSelected(courseToReload.hasCoursePage(CoursePage.SYLLABUS));
        schedulePageCheckBox.setSelected(courseToReload.hasCoursePage(CoursePage.SCHEDULE));
        hwsPageCheckBox.setSelected(courseToReload.hasCoursePage(CoursePage.HWS));
        projectsPageCheckBox.setSelected(courseToReload.hasCoursePage(CoursePage.PROJECTS));

        // THEN THE DATE PICKERS
        LocalDate startDate = courseToReload.getStartingMonday();
        startDatePicker.setValue(startDate);
        LocalDate endDate = courseToReload.getEndingFriday();
        endDatePicker.setValue(endDate);

        // THE LECTURE DAY CHECK BOXES
        mondayCheckBox.setSelected(courseToReload.hasLectureDay(DayOfWeek.MONDAY));
        tuesdayCheckBox.setSelected(courseToReload.hasLectureDay(DayOfWeek.TUESDAY));
        wednesdayCheckBox.setSelected(courseToReload.hasLectureDay(DayOfWeek.WEDNESDAY));
        thursdayCheckBox.setSelected(courseToReload.hasLectureDay(DayOfWeek.THURSDAY));
        fridayCheckBox.setSelected(courseToReload.hasLectureDay(DayOfWeek.FRIDAY));
        
        // THE SCHEDULE ITEMS TABLE
       
        // THE LECTURES TABLE
        
        // THE HWS TABLE

        // NOW WE DO WANT TO RESPOND WHEN THE USER INTERACTS WITH OUR CONTROLS
        courseController.enable(true);
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveCourseButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadCourseButton.setDisable(false);
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /**
     * This function loads all the values currently in the user interface
     * into the course argument.
     * 
     * @param course The course to be updated using the data from the UI controls.
     */
    public void updateCourseInfo(Course course) {
        course.setSubject(Subject.valueOf(courseSubjectComboBox.getSelectionModel().getSelectedItem().toString()));
        course.setNumber(Integer.parseInt(courseNumberTextField.getText()));
        course.setSemester(Semester.valueOf(courseSemesterComboBox.getSelectionModel().getSelectedItem().toString()));
        course.setYear((int) courseYearComboBox.getSelectionModel().getSelectedItem());
        course.setTitle(courseTitleTextField.getText());
        Instructor instructor = course.getInstructor();
        instructor.setName(instructorNameTextField.getText());
        instructor.setHomepageURL(instructorURLTextField.getText());
        updatePageUsingCheckBox(indexPageCheckBox, course, CoursePage.INDEX);
        updatePageUsingCheckBox(syllabusPageCheckBox, course, CoursePage.SYLLABUS);
        updatePageUsingCheckBox(schedulePageCheckBox, course, CoursePage.SCHEDULE);
        updatePageUsingCheckBox(hwsPageCheckBox, course, CoursePage.HWS);
        updatePageUsingCheckBox(projectsPageCheckBox, course, CoursePage.PROJECTS);
        course.setStartingMonday(startDatePicker.getValue());
        course.setEndingFriday(endDatePicker.getValue());
        course.selectLectureDay(DayOfWeek.MONDAY, mondayCheckBox.isSelected());
        course.selectLectureDay(DayOfWeek.TUESDAY, tuesdayCheckBox.isSelected());
        course.selectLectureDay(DayOfWeek.WEDNESDAY, wednesdayCheckBox.isSelected());
        course.selectLectureDay(DayOfWeek.THURSDAY, thursdayCheckBox.isSelected());
        course.selectLectureDay(DayOfWeek.FRIDAY, fridayCheckBox.isSelected());
    }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        progressDialog = new ProgressDialog(primaryStage, "Preparing");
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.NEW_COURSE_ICON, CSB_PropertyType.NEW_COURSE_TOOLTIP, false);
        loadCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.LOAD_COURSE_ICON, CSB_PropertyType.LOAD_COURSE_TOOLTIP, false);
        saveCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.SAVE_COURSE_ICON, CSB_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXPORT_PAGE_ICON, CSB_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXIT_ICON, CSB_PropertyType.EXIT_TOOLTIP, false);
    }
    
    private void initDraftToolbar() {
         draftToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
    fantasyTeamsButton = initChildButton(draftToolbarPane, CSB_PropertyType.FANTASY_TEAMS_ICON, CSB_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
    playersButton = initChildButton(draftToolbarPane, CSB_PropertyType.PLAYERS_ICON, CSB_PropertyType.PLAYERS_TOOLTIP, false);
    standingsButton = initChildButton(draftToolbarPane, CSB_PropertyType.FANTASY_STANDINGS_ICON, CSB_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
    draftButton = initChildButton(draftToolbarPane, CSB_PropertyType.DRAFT_ICON, CSB_PropertyType.DRAFT_TOOLTIP, false);
    mlbButton = initChildButton(draftToolbarPane, CSB_PropertyType.MLB_TEAMS_ICON, CSB_PropertyType.MLB_TEAMS_TOOLTIP, false);
    
    }
    

    // CREATES AND SETS UP ALL THE CONTROLS TO GO IN THE APP WORKSPACE
    private void initWorkspace(ArrayList<String> subjects) throws IOException {
        // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        initBasicCourseInfoControls(subjects);

        // THIS IS FOR SELECTING PAGE LINKS TO INCLUDE
        initPageSelectionControls();

        // THE TOP WORKSPACE HOLDS BOTH THE BASIC COURSE INFO
        // CONTROLS AS WELL AS THE PAGE SELECTION CONTROLS
        initTopWorkspace();

        // THIS IS FOR MANAGING SCHEDULE EDITING
        initScheduleItemsControls();

        // THIS HOLDS ALL OUR WORKSPACE COMPONENTS, SO NOW WE MUST
        // ADD THE COMPONENTS WE'VE JUST INITIALIZED
        workspacePane = new BorderPane();
//        workspacePane.setTop(topWorkspacePane);
//        workspacePane.setCenter(playersPane);
        workspacePane.setCenter(fantasyTeamPane);
        workspacePane.setBottom(draftToolbarPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.setContent(workspacePane);
        workspaceScrollPane.setFitToWidth(true);
        workspaceScrollPane.setFitToHeight(true);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }
    
    // INITIALIZES THE TOP PORTION OF THE WORKWPACE UI
    private void initTopWorkspace() {
        // HERE'S THE SPLIT PANE, ADD THE TWO GROUPS OF CONTROLS
        topWorkspaceSplitPane = new SplitPane();
        topWorkspaceSplitPane.getItems().add(courseInfoPane);
        topWorkspaceSplitPane.getItems().add(pagesSelectionPane);

        // THE TOP WORKSPACE PANE WILL ONLY DIRECTLY HOLD 2 THINGS, A LABEL
        // AND A SPLIT PANE, WHICH WILL HOLD 2 ADDITIONAL GROUPS OF CONTROLS
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(CLASS_BORDERED_PANE);


        // HERE'S THE LABEL
        courseHeadingLabel = initChildLabel(topWorkspacePane, CSB_PropertyType.FANTASY_TEAM_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        // AND NOW ADD THE SPLIT PANE
        //topWorkspacePane.getChildren().add(topWorkspaceSplitPane);
        
        topPlayersPane = new VBox();
        topPlayersPane.getStyleClass().add(CLASS_BORDERED_PANE);
        playersHeadingLabel = initChildLabel(topPlayersPane, CSB_PropertyType.PLAYER_HEADING_LABEL, CLASS_HEADING_LABEL);

        
        topFantasyStandingsPane = new VBox();
        topFantasyStandingsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fantasyStandingsHeadingLabel = initChildLabel(topFantasyStandingsPane, CSB_PropertyType.FANTASY_STANDINGS_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        topDraftPane = new VBox();
        topDraftPane.getStyleClass().add(CLASS_BORDERED_PANE);
        //draftHeadingLabel = initChildLabel(topDraftPane, CSB_PropertyType.DRAFT_SUMMARY_HEADING_LABEL, CLASS_HEADING_LABEL);
    
        topMlbTeamsPane = new VBox();
        topMlbTeamsPane.getStyleClass().add(CLASS_BORDERED_PANE);
        mlbTeamsHeadingLabel = initChildLabel(topMlbTeamsPane, CSB_PropertyType.MLB_TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);
    }

    // INITIALIZES THE CONTROLS IN THE LEFT HALF OF THE TOP WORKSPACE
    private void initBasicCourseInfoControls(ArrayList<String> subjects) throws IOException {
        // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
        // WE'LL ARRANGE THEM IN THE LEFT SIDE IN A VBox
        courseInfoPane = new GridPane();

        // FIRST THE HEADING LABEL
        courseInfoLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_INFO_LABEL, CLASS_SUBHEADING_LABEL, 0, 0, 4, 1);

        // THEN CONTROLS FOR CHOOSING THE SUBJECT
        courseSubjectLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_SUBJECT_LABEL, CLASS_PROMPT_LABEL, 0, 1, 1, 1);
        courseSubjectComboBox = initGridComboBox(courseInfoPane, 1, 1, 1, 1);
        loadSubjectComboBox(subjects);

        // THEN CONTROLS FOR UPDATING THE COURSE NUMBER
        courseNumberLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_NUMBER_LABEL, CLASS_PROMPT_LABEL, 2, 1, 1, 1);
        courseNumberTextField = initGridTextField(courseInfoPane, SMALL_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 3, 1, 1, 1);

        // THEN THE COURSE SEMESTER
        courseSemesterLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_SEMESTER_LABEL, CLASS_PROMPT_LABEL, 0, 2, 1, 1);
        courseSemesterComboBox = initGridComboBox(courseInfoPane, 1, 2, 1, 1);
        ObservableList<String> semesterChoices = FXCollections.observableArrayList();
        for (Semester s : Semester.values()) {
            semesterChoices.add(s.toString());
        }
        courseSemesterComboBox.setItems(semesterChoices);

        // THEN THE COURSE YEAR
        courseYearLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_YEAR_LABEL, CLASS_PROMPT_LABEL, 2, 2, 1, 1);
        courseYearComboBox = initGridComboBox(courseInfoPane, 3, 2, 1, 1);
        ObservableList<Integer> yearChoices = FXCollections.observableArrayList();
        for (int i = LocalDate.now().getYear(); i <= LocalDate.now().getYear() + 1; i++) {
            yearChoices.add(i);
        }
        courseYearComboBox.setItems(yearChoices);

        // THEN THE COURSE TITLE
        courseTitleLabel = initGridLabel(courseInfoPane, CSB_PropertyType.COURSE_TITLE_LABEL, CLASS_PROMPT_LABEL, 0, 3, 1, 1);
        courseTitleTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 3, 3, 1);

        // THEN THE INSTRUCTOR NAME
        instructorNameLabel = initGridLabel(courseInfoPane, CSB_PropertyType.INSTRUCTOR_NAME_LABEL, CLASS_PROMPT_LABEL, 0, 4, 1, 1);
        instructorNameTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 4, 3, 1);

        // AND THE INSTRUCTOR HOMEPAGE
        instructorURLLabel = initGridLabel(courseInfoPane, CSB_PropertyType.INSTRUCTOR_URL_LABEL, CLASS_PROMPT_LABEL, 0, 5, 1, 1);
        instructorURLTextField = initGridTextField(courseInfoPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 1, 5, 3, 1);
    }

    // INITIALIZES THE CONTROLS IN THE RIGHT HALF OF THE TOP WORKSPACE
    private void initPageSelectionControls() {
        // THESE ARE THE CONTROLS FOR SELECTING WHICH PAGES THE SCHEDULE
        // PAGE WILL HAVE TO LINK TO
        pagesSelectionPane = new VBox();
        pagesSelectionPane.getStyleClass().add(CLASS_SUBJECT_PANE);
        pagesSelectionLabel = initChildLabel(pagesSelectionPane, CSB_PropertyType.PAGES_SELECTION_HEADING_LABEL, CLASS_SUBHEADING_LABEL);
        indexPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.INDEX_PAGE);
        syllabusPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.SYLLABUS_PAGE);
        schedulePageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.SCHEDULE_PAGE);
        hwsPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.HWS_PAGE);
        projectsPageCheckBox = initChildCheckBox(pagesSelectionPane, CourseSiteExporter.PROJECTS_PAGE);
    }
    
    // INITIALIZE THE SCHEDULE ITEMS CONTROLS
    private void initScheduleItemsControls() throws IOException {
        // FOR THE LEFT
        dateBoundariesPane = new GridPane();
        dateBoundariesLabel = initGridLabel(dateBoundariesPane, CSB_PropertyType.DATE_BOUNDARIES_LABEL, CLASS_SUBHEADING_LABEL, 0, 0, 1, 1);
        startDateLabel = initGridLabel(dateBoundariesPane, CSB_PropertyType.STARTING_MONDAY_LABEL, CLASS_PROMPT_LABEL, 0, 1, 1, 1);
        startDatePicker = initGridDatePicker(dateBoundariesPane, 1, 1, 1, 1);
        endDateLabel = initGridLabel(dateBoundariesPane, CSB_PropertyType.ENDING_FRIDAY_LABEL, CLASS_PROMPT_LABEL, 0, 2, 1, 1);
        endDatePicker = initGridDatePicker(dateBoundariesPane, 1, 2, 1, 1);

        // THIS ONE IS ON THE RIGHT
        lectureDaySelectorPane = new VBox();
        lectureDaySelectLabel = initChildLabel(lectureDaySelectorPane, CSB_PropertyType.LECTURE_DAY_SELECT_LABEL, CLASS_SUBHEADING_LABEL);
        mondayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.MONDAY_HEADER);
        tuesdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.TUESDAY_HEADER);
        wednesdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.WEDNESDAY_HEADER);
        thursdayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.THURSDAY_HEADER);
        fridayCheckBox = initChildCheckBox(lectureDaySelectorPane, CourseSiteExporter.FRIDAY_HEADER);

        // THIS SPLITS THE TOP
        splitScheduleInfoPane = new SplitPane();
        splitScheduleInfoPane.getItems().add(dateBoundariesPane);
        splitScheduleInfoPane.getItems().add(lectureDaySelectorPane);
        
        // NOW THE CONTROLS FOR ADDING SCHEDULE ITEMS
        scheduleItemsBox = new VBox();
        scheduleItemsToolbar = new HBox();
        
        radioButtonsPane = new GridPane();
        searchPlayerPane = new GridPane();
        
        radioButtonsToolbar = new HBox();
        searchPlayerToolbar = new HBox();
        
        scheduleItemsLabel = initLabel(CSB_PropertyType.PLAYER_HEADING_LABEL, CLASS_HEADING_LABEL);
        addScheduleItemButton = initChildButton(scheduleItemsToolbar, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removeScheduleItemButton = initChildButton(scheduleItemsToolbar, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        
        searchPlayerLabel = initGridLabel(searchPlayerPane, CSB_PropertyType.PLAYER_SEARCH_LABEL, CLASS_PROMPT_LABEL, 4, 1, 1, 1);
        searchPlayerTextField = initGridTextField(searchPlayerPane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 5, 1, 1, 1);
        
        all = initRadioButton(radioButtonsPane, "All      ", 0, 3 , 1, 1);
        c = initRadioButton(radioButtonsPane, "C      ", 1, 3 , 1, 1);
        oneB = initRadioButton(radioButtonsPane, "1B     ", 2, 3 , 1, 1);
        ci = initRadioButton(radioButtonsPane, "CI      ", 3, 3 , 1, 1);
        threeB = initRadioButton(radioButtonsPane, "3B      ", 4, 3 , 1, 1);
        twoB = initRadioButton(radioButtonsPane, "2B      ", 5, 3 , 1, 1);
        mi = initRadioButton(radioButtonsPane, "MI      ", 6, 3 , 1, 1);
        ss = initRadioButton(radioButtonsPane, "SS      ", 7, 3 , 1, 1);
        of = initRadioButton(radioButtonsPane, "OF      ", 8, 3 , 1, 1);
        u = initRadioButton(radioButtonsPane, "U      ", 9, 3 , 1, 1);
        p = initRadioButton(radioButtonsPane, "P      ", 10, 3 , 1, 1);
        
        group = new ToggleGroup();
        all.setToggleGroup(group);
        all.setSelected(true);
        c.setToggleGroup(group);
        oneB.setToggleGroup(group);
        ci.setToggleGroup(group);
        threeB.setToggleGroup(group);
        twoB.setToggleGroup(group);
        mi.setToggleGroup(group);
        ss.setToggleGroup(group);
        of.setToggleGroup(group);
        u.setToggleGroup(group);
        p.setToggleGroup(group);
        
        radioButtonsToolbar.getChildren().add(radioButtonsPane);
        searchPlayerToolbar.getChildren().add(searchPlayerPane);
//        scheduleItemsToolbar.getChildren().add(searchPlayerLabel);
        
        scheduleItemsTable = new TableView();
        scheduleItemsBox.getChildren().add(scheduleItemsLabel);
        scheduleItemsBox.getChildren().add(scheduleItemsToolbar);
        scheduleItemsBox.getChildren().add(searchPlayerToolbar);
        scheduleItemsBox.getChildren().add(radioButtonsToolbar);
        scheduleItemsBox.getChildren().add(scheduleItemsTable);
        scheduleItemsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // NOW SETUP THE TABLE COLUMNS      
        firstNameColumn = new TableColumn(COL_FIRST);
        lastNameColumn = new TableColumn(COL_LAST);
        proTeamColumn = new TableColumn(COL_TEAM);
        positionsColumn = new TableColumn(COL_POSITION);
        birthYearColumn = new TableColumn(COL_BIRTHYEAR);
        runsWinsColumn = new TableColumn(COL_R_W);
        homeRunsSavesColumn = new TableColumn(COL_HR_SV);
        runsBattedInStrikeoutsColumn = new TableColumn(COL_RBI_K);
        stolenBasesEarnedRunAverageColumn = new TableColumn(COL_SB_ERA);
        battingAverageWalksHitsInningsPitchedColumn = new TableColumn(COL_BA_WHIP);
        estimatedValueColumn = new TableColumn(COL_ESTIMATED_VALUE);
        notesColumn = new TableColumn(COL_NOTES);
        
        // AND LINK THE COLUMNS TO THE DATA
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("LastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("ProTeam"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        birthYearColumn.setCellValueFactory(new PropertyValueFactory<String, String>("birthYear"));
        runsWinsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("runsWins"));
        homeRunsSavesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("homeRunsSaves"));
        runsBattedInStrikeoutsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("rbiStrikeouts"));
        stolenBasesEarnedRunAverageColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SbEarnedRunAverage"));
        battingAverageWalksHitsInningsPitchedColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BaWalksHitsInningsPitched"));
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        
        scheduleItemsTable.getColumns().add(firstNameColumn);
        scheduleItemsTable.getColumns().add(lastNameColumn);
        scheduleItemsTable.getColumns().add(proTeamColumn);
        scheduleItemsTable.getColumns().add(positionsColumn);
        scheduleItemsTable.getColumns().add(birthYearColumn);
        scheduleItemsTable.getColumns().add(runsWinsColumn);
        scheduleItemsTable.getColumns().add(homeRunsSavesColumn);
        scheduleItemsTable.getColumns().add(runsBattedInStrikeoutsColumn);
        scheduleItemsTable.getColumns().add(stolenBasesEarnedRunAverageColumn);
        scheduleItemsTable.getColumns().add(battingAverageWalksHitsInningsPitchedColumn);
        scheduleItemsTable.getColumns().add(estimatedValueColumn);
        scheduleItemsTable.getColumns().add(notesColumn);
               
        loadPitchers();
        loadHitters();
//       scheduleItemsTable.setItems(playersList);
        scheduleItemsTable.setItems(dataManager.getCourse().getPlayers());
        scheduleItemsTable.setItems(dataManager.getCourse().setPlayerList(playersList));
        
//        scheduleItemsTable.setItems(playersList);
        
//      scheduleItemsTable.setItems(pitcherList);
 
/////////////////////FANTASY TEAMS BOX///////////////////////////////////////////
        fantasyTeamsBox = new VBox();
        fantasyTeamsToolbar = new HBox();
        
        FantasyTeamNamePane = new GridPane();
        FantasyTeamNameToolbar = new HBox();
        
        fantasyTeamLabel = initLabel(CSB_PropertyType.FANTASY_TEAM_HEADING_LABEL, CLASS_HEADING_LABEL);
        startingLineupLabel = initLabel(CSB_PropertyType.STARTING_LINEUP_LABEL, CLASS_SUBHEADING_LABEL);
        taxiSquadLabel = initLabel(CSB_PropertyType.TAXI_SQUAD_LABEL, CLASS_SUBHEADING_LABEL);
        addFantasyTeamButton = initChildButton(fantasyTeamsToolbar, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_FANTASY_TEAM_TOOLTIP, false);
        removeFantasyTeamButton = initChildButton(fantasyTeamsToolbar, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_FANTASY_TEAM_TOOLTIP, false);
        editFantasyTeamButton = initChildButton(fantasyTeamsToolbar, CSB_PropertyType.EDIT_PLAYER_ICON, CSB_PropertyType.EDIT_FANTASY_TEAM_TOOLTIP, false);
        fantasyTeamNameLabel = initChildLabel(fantasyTeamsToolbar, CSB_PropertyType.FANTASY_TEAM_NAME_LABEL, CLASS_PROMPT_LABEL);
        fantasyTeamNameComboBox = new ComboBox();
        fantasyTeamsToolbar.getChildren().add(fantasyTeamNameComboBox);
        
//        fantasyTeamNameComboBox.setItems(scheduleController.getFantasyTeamNames());
        
        fantasyTeamNameComboBox.setItems(dataManager.getCourse().getFantasyTeamNames());
        fantasyTeamNameComboBox.setItems(dataManager.getCourse().setFantasyTeamNamesList(fantasyTeamNames));
        
        
        
        
        draftNameLabel = initGridLabel(FantasyTeamNamePane, CSB_PropertyType.DRAFT_NAME_LABEL, CLASS_PROMPT_LABEL, 4, 1, 1, 1);
        draftNameTextField = initGridTextField(FantasyTeamNamePane, LARGE_TEXT_FIELD_LENGTH, EMPTY_TEXT, true, 5, 1, 1, 1);
        
        FantasyTeamNameToolbar.getChildren().add(FantasyTeamNamePane);
//        scheduleItemsToolbar.getChildren().add(searchPlayerLabel);
        
        fantasyTeamsTable = new TableView();
        taxiSquadTable =  new TableView();
        fantasyTeamsBox.getChildren().add(fantasyTeamLabel);
        fantasyTeamsBox.getChildren().add(FantasyTeamNameToolbar);
        fantasyTeamsBox.getChildren().add(fantasyTeamsToolbar);      
        fantasyTeamsBox.getChildren().add(startingLineupLabel);
        fantasyTeamsBox.getChildren().add(fantasyTeamsTable);
        fantasyTeamsBox.getChildren().add(taxiSquadLabel);
        fantasyTeamsBox.getChildren().add(taxiSquadTable);
        fantasyTeamsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // NOW SETUP THE TABLE COLUMNS    
        ftPositionColumn = new TableColumn(COL_FT_POSITION);
        ftFirstNameColumn = new TableColumn(COL_FT_FIRST);
        ftLastNameColumn = new TableColumn(COL_FT_LAST);
        ftProTeamColumn = new TableColumn(COL_FT_TEAM);
        ftPositionsColumn = new TableColumn(COL_FT_POSITIONS);   
        ftRunsWinsColumn = new TableColumn(COL_FT_R_W);
        ftHomeRunsSavesColumn = new TableColumn(COL_FT_HR_SV);
        ftRunsBattedInStrikeoutsColumn = new TableColumn(COL_FT_RBI_K);
        ftStolenBasesEarnedRunAverageColumn = new TableColumn(COL_SB_ERA);
        ftBattingAverageWalksHitsInningsPitchedColumn = new TableColumn(COL_FT_BA_WHIP);
        ftEstimatedValueColumn = new TableColumn(COL_FT_ESTIMATED_VALUE);
        ftContractColumn = new TableColumn(COL_FT_CONTRACT);
        ftSalaryColumn = new TableColumn(COL_FT_SALARY);
        
        // AND LINK THE COLUMNS TO THE DATA
        ftPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("rolePosition"));
        ftFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        ftLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        ftProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("ProTeam"));
        ftPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        ftRunsWinsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("runsWins"));
        ftHomeRunsSavesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("homeRunsSaves"));
        ftRunsBattedInStrikeoutsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("rbiStrikeouts"));
        ftStolenBasesEarnedRunAverageColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SbEarnedRunAverage"));
        ftBattingAverageWalksHitsInningsPitchedColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BaWalksHitsInningsPitched"));
        ftEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        ftContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        ftSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        
        fantasyTeamsTable.getColumns().add(ftPositionColumn);
        fantasyTeamsTable.getColumns().add(ftFirstNameColumn);
        fantasyTeamsTable.getColumns().add(ftLastNameColumn);
        fantasyTeamsTable.getColumns().add(ftProTeamColumn);
        fantasyTeamsTable.getColumns().add(ftPositionsColumn);   
        fantasyTeamsTable.getColumns().add(ftRunsWinsColumn);
        fantasyTeamsTable.getColumns().add(ftHomeRunsSavesColumn);
        fantasyTeamsTable.getColumns().add(ftRunsBattedInStrikeoutsColumn);
        fantasyTeamsTable.getColumns().add(ftStolenBasesEarnedRunAverageColumn);
        fantasyTeamsTable.getColumns().add(ftBattingAverageWalksHitsInningsPitchedColumn);
        fantasyTeamsTable.getColumns().add(ftEstimatedValueColumn);
        fantasyTeamsTable.getColumns().add(ftContractColumn);
        fantasyTeamsTable.getColumns().add(ftSalaryColumn);
        
        tsPositionColumn = new TableColumn(COL_FT_POSITION);
        tsFirstNameColumn = new TableColumn(COL_FT_FIRST);
        tsLastNameColumn = new TableColumn(COL_FT_LAST);
        tsProTeamColumn = new TableColumn(COL_FT_TEAM);
        tsPositionsColumn = new TableColumn(COL_FT_POSITIONS);   
        tsRunsWinsColumn = new TableColumn(COL_FT_R_W);
        tsHomeRunsSavesColumn = new TableColumn(COL_FT_HR_SV);
        tsRunsBattedInStrikeoutsColumn = new TableColumn(COL_FT_RBI_K);
        tsStolenBasesEarnedRunAverageColumn = new TableColumn(COL_SB_ERA);
        tsBattingAverageWalksHitsInningsPitchedColumn = new TableColumn(COL_FT_BA_WHIP);
        tsEstimatedValueColumn = new TableColumn(COL_FT_ESTIMATED_VALUE);
        tsContractColumn = new TableColumn(COL_FT_CONTRACT);
        tsSalaryColumn = new TableColumn(COL_FT_SALARY);
        
        // AND LINK THE COLUMNS TO THE DATA
        tsPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("rolePosition"));
        tsFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        tsLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        tsProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("ProTeam"));
        tsPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        tsRunsWinsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("runsWins"));
        tsHomeRunsSavesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("homeRunsSaves"));
        tsRunsBattedInStrikeoutsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("rbiStrikeouts"));
        tsStolenBasesEarnedRunAverageColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SbEarnedRunAverage"));
        tsBattingAverageWalksHitsInningsPitchedColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BaWalksHitsInningsPitched"));
        tsEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        tsContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contact"));
        tsSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        
        taxiSquadTable.getColumns().add(tsPositionColumn);
        taxiSquadTable.getColumns().add(tsFirstNameColumn);
        taxiSquadTable.getColumns().add(tsLastNameColumn);
        taxiSquadTable.getColumns().add(tsProTeamColumn);
        taxiSquadTable.getColumns().add(tsPositionsColumn);   
        taxiSquadTable.getColumns().add(tsRunsWinsColumn);
        taxiSquadTable.getColumns().add(tsHomeRunsSavesColumn);
        taxiSquadTable.getColumns().add(tsRunsBattedInStrikeoutsColumn);
        taxiSquadTable.getColumns().add(tsStolenBasesEarnedRunAverageColumn);
        taxiSquadTable.getColumns().add(tsBattingAverageWalksHitsInningsPitchedColumn);
        taxiSquadTable.getColumns().add(tsEstimatedValueColumn);
        taxiSquadTable.getColumns().add(tsContractColumn);
        taxiSquadTable.getColumns().add(tsSalaryColumn);
                
        
//        fantasyTeamsTable.setItems((dataManager.getCourse().getFantasyPlayers()));
//        if (dataManager.getCourse().getFantasyTeamNames().size() != 0) {
//            System.out.print("HELLO");
//            System.out.print(fantasyTeamNameComboBox.getSelectionModel().getSelectedItem().toString());
//            for(int i = 0; i < (dataManager.getCourse().getFantasyTeam().size()); i++)
//            {
//                if((fantasyTeamNameComboBox.getSelectionModel().getSelectedItem().toString()) == (dataManager.getCourse().getFantasyTeam().get(i).getName())) {
//                    fantasyTeamsTable.setItems(dataManager.getCourse().getFantasyTeam().get(i).getRoster());
//                    System.out.print("ELLO");
//                    
//                }
//            }
//        }
            
        
//        fantasyTeamsTable.setItems(dataManager.getCourse().getPlayers());
        ////////////////////////////////////////////////////////////////
        // NOW THE CONTROLS FOR ADDING LECTURES
        
//        lecturesBox = new VBox();
//        lecturesToolbar = new HBox();
//        addLectureButton = initChildButton(lecturesToolbar, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_LECTURE_TOOLTIP, false);
//        removeLectureButton = initChildButton(lecturesToolbar, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_LECTURE_TOOLTIP, false);
//        moveUpLectureButton = initChildButton(lecturesToolbar, CSB_PropertyType.MOVE_UP_ICON, CSB_PropertyType.MOVE_UP_LECTURE_TOOLTIP, false);
//        moveDownLectureButton = initChildButton(lecturesToolbar, CSB_PropertyType.MOVE_DOWN_ICON, CSB_PropertyType.MOVE_DOWN_LECTURE_TOOLTIP, false);
//        lecturesLabel = initLabel(CSB_PropertyType.LECTURES_HEADING_LABEL, CLASS_SUBHEADING_LABEL);
//        lecturesTable = new TableView();
//        lecturesBox.getChildren().add(lecturesLabel);
//        lecturesBox.getChildren().add(lecturesToolbar);
//        lecturesBox.getChildren().add(lecturesTable);
//        lecturesBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // NOW SETUP THE TABLE COLUMNS
//        lectureTopicsColumn = new TableColumn(COL_TOPIC);
//        lectureSessionsColumn = new TableColumn(COL_SESSIONS);
        
        // AND LINK THE COLUMNS TO THE DATA
//        lectureTopicsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
//        lectureSessionsColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("sessions"));
//        lecturesTable.getColumns().add(lectureTopicsColumn);
//        lecturesTable.getColumns().add(lectureSessionsColumn);
//        lecturesTable.setItems(dataManager.getCourse().getLectures());
        
        // AND NOW THE CONTROLS FOR ADDING HWS
//        hwsBox = new VBox();
//        hwsToolbar = new HBox();
//        addHWButton = initChildButton(hwsToolbar, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_HW_TOOLTIP, false);
//        removeHWButton = initChildButton(hwsToolbar, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_HW_TOOLTIP, false);
//        hwsLabel = initLabel(CSB_PropertyType.HWS_HEADING_LABEL, CLASS_SUBHEADING_LABEL);
//        hwsTable = new TableView();
//        hwsBox.getChildren().add(hwsLabel);
//        hwsBox.getChildren().add(hwsToolbar);
//        hwsBox.getChildren().add(hwsTable);
//        hwsBox.getStyleClass().add(CLASS_BORDERED_PANE);
//        
//        // NOW SETUP THE TABLE COLUMNS
//        hwNamesColumn = new TableColumn(COL_NAME);
//        hwTopicsColumn = new TableColumn(COL_TOPICS);
//        hwDatesColumn = new TableColumn(COL_DATE);
        
        // AND LINK THE COLUMNS TO THE DATA
//        hwNamesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
//        hwTopicsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("topics"));
//        hwDatesColumn.setCellValueFactory(new PropertyValueFactory<LocalDate, String>("date"));
//        hwsTable.getColumns().add(hwNamesColumn);
//        hwsTable.getColumns().add(hwTopicsColumn);
//        hwsTable.getColumns().add(hwDatesColumn);
//        hwsTable.setItems(dataManager.getCourse().getAssignments());
        
        // NOW LET'S ASSEMBLE ALL THE CONTAINERS TOGETHER

        // THIS IS FOR STUFF IN THE TOP OF THE SCHEDULE PANE, WE NEED TO PUT TWO THINGS INSIDE
        scheduleInfoPane = new VBox();

        // FIRST OUR SCHEDULE HEADER
        scheduleInfoHeadingLabel = initChildLabel(scheduleInfoPane, CSB_PropertyType.SCHEDULE_HEADING_LABEL, CLASS_HEADING_LABEL);

        // AND THEN THE SPLIT PANE
        scheduleInfoPane.getChildren().add(splitScheduleInfoPane);

        // FINALLY, EVERYTHING IN THIS REGION ULTIMATELY GOES INTO schedulePane
        schedulePane = new VBox();
//        schedulePane.getChildren().add(scheduleInfoPane);
        schedulePane.getChildren().add(scheduleItemsBox);
//        schedulePane.getChildren().add(lecturesBox);
//        schedulePane.getChildren().add(hwsBox);
//        schedulePane.getStyleClass().add(CLASS_BORDERED_PANE);
        fantasyTeamPane = new VBox();
        fantasyTeamPane.getChildren().add(fantasyTeamsBox);
    
    /////////////////////////////////////////////////////////////////////////////////
    
    ///////////////////MLB TEAMS BOX//////////////////////////////////////////////////
        mlbTeamsTable  = new TableView();
        mlbTeamComboBox = new ComboBox();
        mlbTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", 
                "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", 
                "TOR", "WSH");
        mlbTeamToolbar = new HBox();
        mlbTeamsComboBoxLabel = initLabel(CSB_PropertyType.SELECT_PRO_TEAM_LABEL, CLASS_PROMPT_LABEL);
        mlbTeamsHeadingLabel = initLabel(CSB_PropertyType.MLB_TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        mlbPositionColumn = new TableColumn(COL_POSITION);
        mlbFirstNameColumn = new TableColumn(COL_FIRST);
        mlbLastNameColumn = new TableColumn(COL_LAST);
        
        // AND LINK THE COLUMNS TO THE DATA
        mlbPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        mlbFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        mlbLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        
        mlbTeamsTable.getColumns().add(mlbPositionColumn);
        mlbTeamsTable.getColumns().add(mlbFirstNameColumn);
        mlbTeamsTable.getColumns().add(mlbLastNameColumn);
        
        MLBTeamPane = new VBox();
        
        mlbTeamToolbar.getChildren().add(mlbTeamsComboBoxLabel);
        mlbTeamToolbar.getChildren().add(mlbTeamComboBox);
        
        MLBTeamPane.getChildren().add(mlbTeamsHeadingLabel);
        MLBTeamPane.getChildren().add(mlbTeamToolbar);
        MLBTeamPane.getChildren().add(mlbTeamsTable);
    ////////////////////////DRAFT SUMMARY/////////////////////////////////////
        draftPane = new VBox();
        draftToolbar = new HBox();
        draftTable  = new TableView();
        draftHeadingLabel = initLabel(CSB_PropertyType.DRAFT_SUMMARY_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        draftStarButton = initChildButton(draftToolbar, CSB_PropertyType.SELECT_PLAYER_ICON, CSB_PropertyType.SELECT_PLAYER_TOOLTIP, false);
        draftStartButton = initChildButton(draftToolbar, CSB_PropertyType.START_DRAFT_ICON, CSB_PropertyType.START_DRAFT_TOOLTIP, false);
        draftPauseButton = initChildButton(draftToolbar, CSB_PropertyType.PAUSE_DRAFT_ICON, CSB_PropertyType.PAUSE_DRAFT_TOOLTIP, false);       

        draftPickColumn = new TableColumn(COL_D_PICK_NUMBER);    
        draftFirstNameColumn = new TableColumn(COL_D_FIRST);
        draftLastNameColumn = new TableColumn(COL_D_LAST);
        draftTeamColumn = new TableColumn(COL_D_TEAM);
        draftContractColumn = new TableColumn(COL_D_CONTRACT);
        draftSalaryColumn = new TableColumn(COL_D_SALARY);

        draftPickColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        draftFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        draftLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        draftTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("fantasyTeam"));
        draftContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        draftSalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        
        draftTable.getColumns().add(draftPickColumn);
        draftTable.getColumns().add(draftFirstNameColumn);
        draftTable.getColumns().add(draftLastNameColumn);
        draftTable.getColumns().add(draftTeamColumn);
        draftTable.getColumns().add(draftContractColumn);
        draftTable.getColumns().add(draftSalaryColumn);
        
        draftPane.getChildren().add(draftHeadingLabel);
        draftPane.getChildren().add(draftToolbar);
        draftPane.getChildren().add(draftTable);
        
    ///////////////////FANTASY STANDINGS PANE//////////////////////////////////////////////////
        fantasyStandingsPane = new VBox();
        fantasyStandingsTable  = new TableView();
        fantasyStandingsHeadingLabel = initLabel(CSB_PropertyType.FANTASY_STANDINGS_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        fsTeamNameColumn = new TableColumn(COL_FS_TEAM_NAME);
        fsPlayersNeededColumn = new TableColumn(COL_FS_PLAYERS_NEEDED);
        fsMoneyLeftColumn = new TableColumn(COL_FS_MONEY_LEFT);
        fsMoneyPerPlayerColumn = new TableColumn(COL_FS_MONEY_PER_PLAYER);
        fsRColumn = new TableColumn(COL_FS_R);
        fsHrColumn = new TableColumn(COL_FS_HR);
        fsRbiColumn = new TableColumn(COL_FS_RBI);
        fsSbColumn = new TableColumn(COL_FS_SB);
        fsBaColumn = new TableColumn(COL_FS_BA);
        fsWColumn = new TableColumn(COL_FS_W);
        fsSvColumn = new TableColumn(COL_FS_SV);
        fsKColumn = new TableColumn(COL_FS_K);
        fsEraColumn = new TableColumn(COL_FS_ERA);
        fsWhipColumn = new TableColumn(COL_FS_WHIP);
        fsTotalPointsColumn = new TableColumn(COL_FS_TOTAL_POINTS);
        
        fsTeamNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        fsPlayersNeededColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsMoneyLeftColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsMoneyPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsRColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsHrColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsRbiColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsSbColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsBaColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsWColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsSvColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsKColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        fsTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<String, String>(""));
        
        fantasyStandingsTable.getColumns().add(fsTeamNameColumn);
        fantasyStandingsTable.getColumns().add(fsPlayersNeededColumn);
        fantasyStandingsTable.getColumns().add(fsMoneyLeftColumn);
        fantasyStandingsTable.getColumns().add(fsMoneyPerPlayerColumn);
        fantasyStandingsTable.getColumns().add(fsRColumn);
        fantasyStandingsTable.getColumns().add(fsHrColumn);
        fantasyStandingsTable.getColumns().add(fsRbiColumn);
        fantasyStandingsTable.getColumns().add(fsSbColumn);
        fantasyStandingsTable.getColumns().add(fsBaColumn);
        fantasyStandingsTable.getColumns().add(fsWColumn);
        fantasyStandingsTable.getColumns().add(fsSvColumn);
        fantasyStandingsTable.getColumns().add(fsKColumn);
        fantasyStandingsTable.getColumns().add(fsEraColumn);
        fantasyStandingsTable.getColumns().add(fsWhipColumn);
        fantasyStandingsTable.getColumns().add(fsTotalPointsColumn);
        
        fantasyStandingsPane.getChildren().add(fantasyStandingsHeadingLabel);
        fantasyStandingsPane.getChildren().add(fantasyStandingsTable);
        
        fantasyStandingsTable.setItems(dataManager.getCourse().getFantasyTeam());
        
//        
//        // AND LINK THE COLUMNS TO THE DATA
//        mlbPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
//        mlbFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
//        mlbLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
//        
//        mlbTeamsTable.getColumns().add(mlbPositionColumn);
//        mlbTeamsTable.getColumns().add(mlbFirstNameColumn);
//        mlbTeamsTable.getColumns().add(mlbLastNameColumn);
//        
//        MLBTeamPane = new VBox();
//        
//        mlbTeamToolbar.getChildren().add(mlbTeamsComboBoxLabel);
//        mlbTeamToolbar.getChildren().add(mlbTeamComboBox);
//        
//        MLBTeamPane.getChildren().add(mlbTeamsHeadingLabel);
//        MLBTeamPane.getChildren().add(mlbTeamToolbar);
//        MLBTeamPane.getChildren().add(mlbTeamsTable);    
    }
    ///////////////////////////////////////////////////////////////////////////////

    public void loadPitchers() throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        
//        FileChooser courseFileChooser = new FileChooser();
//        courseFileChooser.setInitialDirectory(new File(PATH_PLAYERS)); 
//        File selectedFile = courseFileChooser.showOpenDialog(getWindow());
        
        File selectedFile = new File("./data/players/Pitchers.json");
        
        if (selectedFile != null){

        JsonObject json = loadJSONFile(selectedFile.getAbsolutePath());

//        playersList.clear();
        JsonArray jsonPitchersArray = json.getJsonArray(JSON_PITCHER);
        for (int i = 0; i < jsonPitchersArray.size(); i++) {
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
//            Player pitcher = new Player("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0");
            Player pitcher = new Player();
            
            pitcher.setFirstName(jso.getString(JSON_FIRST));
            pitcher.setLastName(jso.getString(JSON_LAST));
            pitcher.setProTeam(jso.getString(JSON_TEAM));
//            pitcher.setPosition(jso.getString(JSON_TEAM));
            pitcher.setPosition("P");
            pitcher.setNotes(jso.getString(JSON_NOTES));
            pitcher.setBirthYear(jso.getString(JSON_YEAR_OF_BIRTH));
//            pitcher.setEstimatedValue(jso.getString(JSON_TEAM));
            pitcher.setEstimatedValue("$0");
            pitcher.setRunsWins(jso.getString(JSON_W));
            pitcher.setRbiStrikeouts(jso.getString(JSON_K));
            pitcher.setHomeRunsSaves(jso.getString(JSON_SV));
            pitcher.setSbEarnedRunAverage(jso.getString(JSON_ER));
            pitcher.setBaWalksHitsInningsPitched("0");
            
            //IP NOT WHIP
            //pitcher.setWalksHitsInningsPitched(jso.getString(JSON_IP));
            
//            pitcher.setDescription(jso.getString(JSON_SCHEDULE_ITEM_DESCRIPTION));
//            JsonObject jsoDate = jso.getJsonObject(JSON_SCHEDULE_ITEM_DATE);
//            year = jsoDate.getInt(JSON_YEAR);
//            month = jsoDate.getInt(JSON_MONTH);
//            day = jsoDate.getInt(JSON_DAY);            
//            pitcher.setDate(LocalDate.of(year, month, day));
//            pitcher.setLink(jso.getString(JSON_SCHEDULE_ITEM_LINK));
            
            // ADD IT TO THE COURSE
            playersList.add(pitcher);
            noEditPlayersList.add(pitcher);
//            scheduleItemsTable.setItems(playersList);
        }
        }
    }
        public void loadHitters() throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        
//        FileChooser courseFileChooser = new FileChooser();
//        courseFileChooser.setInitialDirectory(new File(PATH_PLAYERS)); 
//        File selectedFile = courseFileChooser.showOpenDialog(getWindow());
        
        File selectedFile = new File("./data/players/Hitters.json");
        
        if (selectedFile != null){

        JsonObject json = loadJSONFile(selectedFile.getAbsolutePath());

//        playersList.clear();
        JsonArray jsonHittersArray = json.getJsonArray(JSON_HITTER);
        for (int i = 0; i < jsonHittersArray.size(); i++) {
            JsonObject jso = jsonHittersArray.getJsonObject(i);
//            Player pitcher = new Player("0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0");
            Player hitter = new Player();
            
            hitter.setFirstName(jso.getString(JSON_FIRST));
            hitter.setLastName(jso.getString(JSON_LAST));
            hitter.setProTeam(jso.getString(JSON_TEAM));
//            hitter.setPosition(jso.getString(JSON_TEAM));
            hitter.setPosition(jso.getString(JSON_QP));
            hitter.setNotes(jso.getString(JSON_NOTES));
            hitter.setBirthYear(jso.getString(JSON_YEAR_OF_BIRTH));
//            hitter.setEstimatedValue(jso.getString(JSON_TEAM));
            hitter.setEstimatedValue("$0");
            hitter.setRunsWins(jso.getString(JSON_R));
            hitter.setRbiStrikeouts(jso.getString(JSON_RBI));
            hitter.setHomeRunsSaves(jso.getString(JSON_HR));
            hitter.setSbEarnedRunAverage(jso.getString(JSON_SB));
            hitter.setBaWalksHitsInningsPitched("0");
            
            //IP NOT WHIP
            //pitcher.setWalksHitsInningsPitched(jso.getString(JSON_IP));
            
//            pitcher.setDescription(jso.getString(JSON_SCHEDULE_ITEM_DESCRIPTION));
//            JsonObject jsoDate = jso.getJsonObject(JSON_SCHEDULE_ITEM_DATE);
//            year = jsoDate.getInt(JSON_YEAR);
//            month = jsoDate.getInt(JSON_MONTH);
//            day = jsoDate.getInt(JSON_DAY);            
//            pitcher.setDate(LocalDate.of(year, month, day));
//            pitcher.setLink(jso.getString(JSON_SCHEDULE_ITEM_LINK));
            
            // ADD IT TO THE COURSE
            playersList.add(hitter);
            noEditPlayersList.add(hitter);
//            scheduleItemsTable.setItems(playersList);
        }
        }
    }
///////    
        private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
        
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    /////////////////////////////////////////////////////////////////////

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Course IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        
        csbPane = new BorderPane();
        csbPane.setTop(fileToolbarPane);
        
        
        
        primaryScene = new Scene(csbPane);
//        fantasyTeamsScene = new Scene(fantasyTeamsPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    // INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, progressDialog, courseFileManager, siteExporter);
        newCourseButton.setOnAction(e -> {
            fileController.handleNewCourseRequest(this);
        });
        loadCourseButton.setOnAction(e -> {

                fileController.handleLoadCourseRequest(this);

        });
        saveCourseButton.setOnAction(e -> {
            fileController.handleSaveCourseRequest(this, dataManager.getCourse());
        });
        exportSiteButton.setOnAction(e -> {
//            fileController.handleExportCourseRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });

        // DRAFT BUTTONS
        
        fantasyTeamsButton.setOnAction(e -> {
            workspacePane.setCenter(fantasyTeamPane);
        });
        
        playersButton.setOnAction(e -> {
//          workspacePane.setCenter(topPlayersPane);
            workspacePane.setCenter(schedulePane);
        });
        
        standingsButton.setOnAction(e -> {
            workspacePane.setCenter(fantasyStandingsPane);
        });
        
        draftButton.setOnAction(e -> {
            workspacePane.setCenter(draftPane);
        });
        
        mlbButton.setOnAction(e -> {
            workspacePane.setCenter(MLBTeamPane);
        });
        
        //RADIO BUTTONS
        
        all.setOnAction(e -> {
        scheduleItemsTable.setItems(dataManager.getCourse().getPlayers());
        scheduleItemsTable.setItems(dataManager.getCourse().setPlayerList(playersList));
        });
        
        c.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("C"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        oneB.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("1B"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        ci.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("1B") ||
                        playersList.get(i).getPosition().contains("3B"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        threeB.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("3B"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        twoB.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("2B"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        mi.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("2B") || 
                        playersList.get(i).getPosition().contains("SS"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        ss.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("SS"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        of.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition().contains("OF"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        u.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (!(playersList.get(i).getPosition() == "P"))
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        p.setOnAction(e -> {
            radioPlayersList.clear();
            for(int i = 0; i < playersList.size(); i++)
            {
                if (playersList.get(i).getPosition() == "P")
                        {
                            radioPlayersList.add(playersList.get(i));
                        }
            }
        scheduleItemsTable.setItems(radioPlayersList);    
        });
        
        // THEN THE COURSE EDITING CONTROLS
        courseController = new CourseEditController();
        
        fantasyTeamNameComboBox.setOnAction(e -> {
            if (dataManager.getCourse().getFantasyTeamNames().size() != 0) {
//            System.out.print("HELLO");
//            System.out.print(fantasyTeamNameComboBox.getSelectionModel().getSelectedItem().toString());
            for(int i = 0; i < (dataManager.getCourse().getFantasyTeam().size()); i++)
            {
                if((fantasyTeamNameComboBox.getSelectionModel().getSelectedItem().toString()) == (dataManager.getCourse().getFantasyTeam().get(i).getName())) {
                    fantasyTeamsTable.setItems(dataManager.getCourse().getFantasyTeam().get(i).getRoster());
                    taxiSquadTable.setItems(dataManager.getCourse().getFantasyTeam().get(i).getTaxi());
//                    System.out.print("ELLO");
//                    System.out.print(dataManager.getCourse().getFantasyTeam().get(i).getName());
//                    System.out.println(dataManager.getCourse().getFantasyTeam().get(i).getRoster());
                    
                }
            }
        }
        }); 
        
        mlbTeamComboBox.setOnAction(e -> {
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "ATL") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("ATL")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }    
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "AZ") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("AZ")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "CHC") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("CHC")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "CIN") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("CIN")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "COL") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("COL")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "LAD") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("LAD")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "MIA") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("MIA")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "MIL") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("MIL")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "NYM") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("NYM")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "PHI") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("PHI")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "PIT") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("PIT")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "SD") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("SD")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "SF") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("SF")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "STL") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("STL")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "TOR") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("TOR")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
            if((mlbTeamComboBox.getSelectionModel().getSelectedItem().toString()) == "WSH") {
                mlbTeamList.clear();
                for(int i = 0; i < noEditPlayersList.size(); i++) {
                    if (noEditPlayersList.get(i).getProTeam().contains("WSH")) {
                        mlbTeamList.add(noEditPlayersList.get(i));
                    }
                }
                mlbTeamsTable.setItems(mlbTeamList);             
            }
        });
        
        
        courseSubjectComboBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        courseSemesterComboBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        courseYearComboBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        indexPageCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        syllabusPageCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        schedulePageCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        hwsPageCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        projectsPageCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });

        // TEXT FIELDS HAVE A DIFFERENT WAY OF LISTENING FOR TEXT CHANGES
        registerTextFieldController(courseNumberTextField);
        registerTextFieldController(courseTitleTextField);
        registerTextFieldController(instructorNameTextField);
        registerTextFieldController(instructorURLTextField);

        // THE DATE SELECTION ONES HAVE PARTICULAR CONCERNS, AND SO
        // GO THROUGH A DIFFERENT METHOD
        startDatePicker.setOnAction(e -> {
            courseController.handleDateSelectionRequest(this, startDatePicker, endDatePicker);
        });
        endDatePicker.setOnAction(e -> {
            courseController.handleDateSelectionRequest(this, startDatePicker, endDatePicker);
        });

        // AND THE LECTURE DAYS CHECKBOXES
        mondayCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        tuesdayCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        wednesdayCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        thursdayCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        fridayCheckBox.setOnAction(e -> {
            courseController.handleCourseChangeRequest(this);
        });
        
        // AND NOW THE SCHEDULE ITEM ADDING AND EDITING CONTROLS
        scheduleController = new ScheduleEditController(primaryStage, dataManager.getCourse(), messageDialog, yesNoCancelDialog);
        addScheduleItemButton.setOnAction(e -> {
            scheduleController.handleAddPlayerRequest(this);
        });
        removeScheduleItemButton.setOnAction(e -> {
//            scheduleController.handleRemoveScheduleItemRequest(this, scheduleItemsTable.getSelectionModel().getSelectedItem());
           scheduleController.handleRemovePlayerRequest(this, scheduleItemsTable.getSelectionModel().getSelectedItem());
        });
        addFantasyTeamButton.setOnAction(e -> {
           scheduleController.handleAddFantasyTeamRequest(this);
        });
       
        removeFantasyTeamButton.setOnAction(e -> {
           scheduleController.handleRemoveFantasyTeamRequest(this, fantasyTeamNameComboBox.getSelectionModel().getSelectedItem().toString());
        });
//        addLectureButton.setOnAction(e -> {
//            scheduleController.handleAddLectureRequest(this);
//        });
//        removeLectureButton.setOnAction(e-> {
//            scheduleController.handleRemoveLectureRequest(this, lecturesTable.getSelectionModel().getSelectedItem());
//        });
//        moveUpLectureButton.setOnAction(e -> {
//            scheduleController.handleMoveUpLectureRequest(this, lecturesTable.getSelectionModel().getSelectedItem());
//        });
//        moveDownLectureButton.setOnAction(e -> {
//            scheduleController.handleMoveDownLectureRequest(this, lecturesTable.getSelectionModel().getSelectedItem());
//        });
//        addHWButton.setOnAction(e -> {
//            scheduleController.handleAddAssignmentRequest(this);
//        });
//        removeHWButton.setOnAction(e -> {
//            scheduleController.handleRemoveAssignmentRequest(this, hwsTable.getSelectionModel().getSelectedItem());
//        });

        // AND NOW THE SCHEDULE ITEMS TABLE
        scheduleItemsTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player pl = scheduleItemsTable.getSelectionModel().getSelectedItem();
                //scheduleController.handleEditPlayerRequest(this, pl);
                String ftName = scheduleController.handleEditPlayerRequest(this, pl);
                if(fantasyTeamNames.size() != 0) {
                    
                    for(int i = 0; i < fantasyTeamNames.size(); i++) {
                        if((ftName) == (dataManager.getCourse().getFantasyTeam().get(i).getName())){
//                            System.out.print(scheduleController.handleEditPlayerRequest(this, pl) + " | ");
//                            System.out.print(dataManager.getCourse().getFantasyTeam().get(i).getName() + " | ");
//                            System.out.print(dataManager.getCourse().getFantasyPlayers());
                            tempList = dataManager.getCourse().getFantasyPlayers();
//                            String tempContract = dataManager.getCourse().getFantasyPlayers().get(i).getContract();
//                            dataManager.getCourse().getFantasyPlayers().get(i).setContract(tempContract);
                            dataManager.getCourse().getFantasyTeam().get(i).addRoster(tempList);
                            //dataManager.getCourse().clearFantasyPlayers();
                            
                            
                            
//                            System.out.print("Fantasy Players: ");
//                            System.out.println(dataManager.getCourse().getFantasyPlayers());
//                            System.out.print(dataManager.getCourse().getFantasyTeam().get(i).getName() + ": ");
//                            System.out.println(dataManager.getCourse().getFantasyTeam().get(i).getRoster());
                        } 
                    }
                    dataManager.getCourse().clearFantasyPlayers();
//                    System.out.println("cleared");
                }
            }
        });        
        
        // AND NOW THE SCHEDULE ITEMS TABLE
//        scheduleItemsTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2) {
//                // OPEN UP THE SCHEDULE ITEM EDITOR
//                ScheduleItem si = scheduleItemsTable.getSelectionModel().getSelectedItem();
//                scheduleController.handleEditScheduleItemRequest(this, si);
//            }
//        });
        
        // AND THE LECTURES TABLE
//        lecturesTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2) {
//                // OPEN UP THE LECTURE EDITOR
//                Lecture l = lecturesTable.getSelectionModel().getSelectedItem();
//                scheduleController.handleEditLectureRequest(this, l);
//            }
//        });
        
        // AND THE HWS TABLE
//        hwsTable.setOnMouseClicked(e -> {
//            if (e.getClickCount() == 2) {
//                // OPEN UP THE HWS EDITOR
//                Assignment a = hwsTable.getSelectionModel().getSelectedItem();
//                scheduleController.handleEditAssignmentRequest(this, a);
//            }
//        });
    }

    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD
    private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            courseController.handleCourseChangeRequest(this);
        });
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, CSB_PropertyType icon, CSB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    // INIT A RADIO BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private RadioButton initRadioButton(GridPane toolbar, String initText, int col, int row, int colSpan, int rowSpan){
        RadioButton radioButton = new RadioButton(initText);
        toolbar.add(radioButton, col, row, colSpan, rowSpan);
        return radioButton;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(CSB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, CSB_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, CSB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A COMBO BOX AND PUT IT IN A GridPane
    private ComboBox initGridComboBox(GridPane container, int col, int row, int colSpan, int rowSpan) throws IOException {
        ComboBox comboBox = new ComboBox();
        container.add(comboBox, col, row, colSpan, rowSpan);
        return comboBox;
    }

    // LOAD THE COMBO BOX TO HOLD Course SUBJECTS
    private void loadSubjectComboBox(ArrayList<String> subjects) {
        for (String s : subjects) {
            courseSubjectComboBox.getItems().add(s);
        }
    }

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    private TextField initGridTextField(GridPane container, int size, String initText, boolean editable, int col, int row, int colSpan, int rowSpan) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.add(tf, col, row, colSpan, rowSpan);
        return tf;
    }

    // INIT A DatePicker AND PUT IT IN A GridPane
    private DatePicker initGridDatePicker(GridPane container, int col, int row, int colSpan, int rowSpan) {
        DatePicker datePicker = new DatePicker();
        container.add(datePicker, col, row, colSpan, rowSpan);
        return datePicker;
    }

    // INIT A CheckBox AND PUT IT IN A TOOLBAR
    private CheckBox initChildCheckBox(Pane container, String text) {
        CheckBox cB = new CheckBox(text);
        container.getChildren().add(cB);
        return cB;
    }

    // INIT A DatePicker AND PUT IT IN A CONTAINER
    private DatePicker initChildDatePicker(Pane container) {
        DatePicker dp = new DatePicker();
        container.getChildren().add(dp);
        return dp;
    }
    
    // LOADS CHECKBOX DATA INTO A Course OBJECT REPRESENTING A CoursePage
    private void updatePageUsingCheckBox(CheckBox cB, Course course, CoursePage cP) {
        if (cB.isSelected()) {
            course.selectPage(cP);
        } else {
            course.unselectPage(cP);
        }
    }    
}
