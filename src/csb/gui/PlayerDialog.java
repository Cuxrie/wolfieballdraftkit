package csb.gui;

import csb.data.Lecture;
import csb.data.Player;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Victor Cheng
 */
public class PlayerDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    
    BorderPane borderPane;
    BorderPane innerBorderPane;
    VBox innerVBox;
    VBox editComboboxBox;
    
    GridPane gridPaneT;
    
    Label firstNameLabel;
    Label lastNameLabel;
    TextField lastNameTextField;
    TextField firstNameTextField;
            
    Label proTeamLabel;
    ComboBox proTeamComboBox;
       
    Label sessionsLabel;
    ComboBox sessionsComboBox;
    
    Label fantasyTeamLabel;
    ComboBox fantasyTeamComboBox;
    HBox fantasyTeamToolbar;
    Label rolePositionLabel;
    ComboBox rolePositionComboBox;
    HBox positionToolbar;
    Label contractLabel;
    ComboBox contractComboBox;
    HBox contractToolbar;
    Label salaryLabel;
    TextField salaryTextField;
    HBox salaryToolbar;
    
    HBox buttonsToolbar;
    
    CheckBox posCCheckBox;
    CheckBox posOneBCheckBox;
    CheckBox posTwoBCheckBox;
    CheckBox posThreeBCheckBox;
    CheckBox posScCheckBox;
    CheckBox posOfCheckBox;
    CheckBox posPCheckBox;
    
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    ObservableList<String> fantasyTeamNames;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    
    public static final String LAST_NAME_PROMPT = "Last Name: ";
    public static final String FIRST_NAME_PROMPT = "First Name: ";
    public static final String PRO_TEAM_PROMPT = "Pro Team: ";
    
    public static final String FANTASY_TEAM_PROMPT = "Fantasy Team: ";
    public static final String POSITION_PROMPT = "Position: ";
    public static final String CONTRACT_PROMPT = "Contract: ";
    public static final String SALARY_PROMPT = "Salary ($): ";
    
    public static final String SESSIONS_PROMPT = "Number of Sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    
    public static final String PLAYER_HEADING = "Player Details";
    
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    
    public static final String ADD_PLAYER_TITLE = "Add New Player";
    
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public PlayerDialog(Stage primaryStage) {
        // FIRST MAKE OUR LECTURE AND INITIALIZE
        // IT WITH DEFAULT VALUES
        player = new Player();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC 
        lastNameLabel = new Label(LAST_NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
        
        // AND THE NUMBER OF SESSIONS
        firstNameLabel = new Label(FIRST_NAME_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });
        
        proTeamLabel = new Label(PRO_TEAM_PROMPT);
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamComboBox = new ComboBox();
        proTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", 
                "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", 
                "TOR", "WSH");
        proTeamComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//                String proTeam = (newValue.toString());
                player.setProTeam(newValue.toString());
            }
            
        });


        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        posCCheckBox = new CheckBox("C");
        posOneBCheckBox = new CheckBox("1B");
        posTwoBCheckBox = new CheckBox("2B");
        posThreeBCheckBox = new CheckBox("3B");
        posScCheckBox = new CheckBox("SC");
        posOfCheckBox = new CheckBox("OF");
        posPCheckBox = new CheckBox("P");
        posCCheckBox.setOnAction(e -> {player.setPosition("C");});
        posOneBCheckBox.setOnAction(e -> {player.setPosition("1B");});
        posTwoBCheckBox.setOnAction(e -> {player.setPosition("2B");});
        posThreeBCheckBox.setOnAction(e -> {player.setPosition("3B");});
        posScCheckBox.setOnAction(e -> {player.setPosition("SC");});
        posOfCheckBox.setOnAction(e -> {player.setPosition("OF");});
        posPCheckBox.setOnAction(e -> {player.setPosition("P");});
        
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
        gridPane.add(lastNameLabel,        0, 1, 1, 1);
        gridPane.add(lastNameTextField,    1, 1, 1, 1);
        gridPane.add(firstNameLabel,     0, 2, 1, 1);
        gridPane.add(firstNameTextField,  1, 2, 1, 1);
        gridPane.add(proTeamLabel,     0, 3, 1, 1);
        gridPane.add(proTeamComboBox,  1, 3, 1, 1);        
        gridPane.add(posCCheckBox, 0, 4 , 1, 1);
        gridPane.add(posOneBCheckBox, 1, 4 , 1, 1);
        gridPane.add(posTwoBCheckBox, 2, 4 , 1, 1);
        gridPane.add(posThreeBCheckBox, 3, 4 , 1, 1);
        gridPane.add(posScCheckBox, 4, 4 , 1, 1);
        gridPane.add(posOfCheckBox, 5, 4 , 1, 1);
        gridPane.add(posPCheckBox, 6, 4 , 1, 1);
        
        gridPane.add(completeButton,    0, 5, 1, 1);
        gridPane.add(cancelButton,      1, 5, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
///////////////////////////EDIT DIALOG/////////////////////////////////////////////////////
//    public PlayerDialog(Stage primaryStage, String overload) {
//        // FIRST MAKE OUR LECTURE AND INITIALIZE
//        // IT WITH DEFAULT VALUES
//        player = new Player();
//        
//        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
//        // FOR IT WHEN IT IS DISPLAYED
//        initModality(Modality.WINDOW_MODAL);
//        initOwner(primaryStage);
//        
//        // FIRST OUR CONTAINER
////        gridPane = new GridPane();
////        gridPane.setPadding(new Insets(10, 20, 20, 20));
////        gridPane.setHgap(10);
////        gridPane.setVgap(10);
//        
//        borderPane = new BorderPane();
//        borderPane.setPadding(new Insets(10, 20, 20, 20));
//
//        innerBorderPane = new BorderPane();
//        innerVBox = new VBox();
//        editComboboxBox = new VBox();
//        buttonsToolbar = new HBox();
//        
//        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
//        // ON WHETHER WE'RE ADDING OR EDITING
//        headingLabel = new Label(PLAYER_HEADING);
//        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
//    
//        // NOW THE TOPIC
//        fantasyTeamToolbar = new HBox();
//        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
//        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        fantasyTeamComboBox = new ComboBox();
//        fantasyTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", 
//                "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", 
//                "TOR", "WSH");
//        fantasyTeamComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
////                String proTeam = (newValue.toString());
//                player.setProTeam(newValue.toString());
//            }
//            
//        });
//        fantasyTeamToolbar.getChildren().add(fantasyTeamLabel);
//        fantasyTeamToolbar.getChildren().add(fantasyTeamComboBox);
//        
//        positionToolbar = new HBox();
//        rolePositionLabel = new Label(POSITION_PROMPT);
//        rolePositionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        rolePositionComboBox = new ComboBox();
//        rolePositionComboBox.getItems().addAll("C", "1B", "CI", "3B", "2B", "MI", "SS", "OF", "U", "P");
//        rolePositionComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
////                String proTeam = (newValue.toString());
//                player.setRolePosition(newValue.toString());
//            }
//            
//        }); 
//        positionToolbar.getChildren().add(rolePositionLabel);
//        positionToolbar.getChildren().add(rolePositionComboBox);
//        
//        contractToolbar = new HBox();
//        contractLabel = new Label(CONTRACT_PROMPT);
//        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        contractComboBox = new ComboBox();
//        contractComboBox.getItems().addAll("S2", "S1", "X");
//        contractComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
////                String proTeam = (newValue.toString());
//                player.setContract(newValue.toString());
//            }
//            
//        });
//        contractToolbar.getChildren().add(contractLabel);
//        contractToolbar.getChildren().add(contractComboBox);
//        
//        // AND THE NUMBER OF SESSIONS
//        salaryToolbar = new HBox();
//        salaryLabel = new Label(SALARY_PROMPT);
//        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
//        salaryTextField = new TextField();
//        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
////            player.setSalary(newValue);
//            player.setSalary(Double.parseDouble(salaryTextField.getText()));
//        });
//        salaryToolbar.getChildren().add(salaryLabel);
//        salaryToolbar.getChildren().add(salaryTextField);
//
//        // AND FINALLY, THE BUTTONS
//        completeButton = new Button(COMPLETE);
//        cancelButton = new Button(CANCEL);
//        
//        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
//        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
//            Button sourceButton = (Button)ae.getSource();
//            PlayerDialog.this.selection = sourceButton.getText();
//            PlayerDialog.this.hide();
//        };
//        
//        completeButton.setOnAction(completeCancelHandler);
//        cancelButton.setOnAction(completeCancelHandler);
//
//        // NOW LET'S ARRANGE THEM ALL AT ONCE
////        editComboboxBox.getChildren().add(fantasyTeamToolbar);
//        editComboboxBox.getChildren().add(positionToolbar);
//        editComboboxBox.getChildren().add(contractToolbar);
//        editComboboxBox.getChildren().add(salaryToolbar);
//        borderPane.setCenter(editComboboxBox);
//            
//        buttonsToolbar.getChildren().add(completeButton);
//        buttonsToolbar.getChildren().add(cancelButton);
//        borderPane.setBottom(buttonsToolbar);
//
//        // AND PUT THE GRID PANE IN THE WINDOW
//        dialogScene = new Scene(borderPane);
//        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
//        this.setScene(dialogScene);
//    }    
//    
/////////////////////////////////////////////////////////////////////////
    public PlayerDialog(Stage primaryStage, String overload) {
        // FIRST MAKE OUR LECTURE AND INITIALIZE
        // IT WITH DEFAULT VALUES
        player = new Player();
        fantasyTeamNames = FXCollections.observableArrayList();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE TOPIC 
        lastNameLabel = new Label(FANTASY_TEAM_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamComboBox = new ComboBox(fantasyTeamNames);
        fantasyTeamComboBox.getItems().addAll();
        fantasyTeamComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//                String proTeam = (newValue.toString());
                player.setFantasyTeam(newValue.toString());
            }
            
        });
        
        // AND THE NUMBER OF SESSIONS
        rolePositionLabel = new Label(POSITION_PROMPT);
        rolePositionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        rolePositionComboBox = new ComboBox();
        rolePositionComboBox.getItems().addAll("C", "1B", "CI", "3B", "2B", "MI", "SS", "OF", "U", "P");
        rolePositionComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//                String proTeam = (newValue.toString());
                player.setRolePosition(newValue.toString());
            }
            
        });
        
        contractLabel = new Label(CONTRACT_PROMPT);
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = new ComboBox();
        contractComboBox.getItems().addAll("S2", "S1", "X");
        contractComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//                String proTeam = (newValue.toString());
                player.setContract(newValue.toString());
            }
            
        });
        
        salaryLabel = new Label(SALARY_PROMPT);
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setSalary(Double.parseDouble(salaryTextField.getText()));
        });


        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel,      0, 0, 2, 1);
//        gridPane.add(lastNameLabel,     0, 1, 1, 1);
//        gridPane.add(lastNameTextField,    1, 1, 1, 1);
        gridPane.add(fantasyTeamLabel,     0, 2, 1, 1);
        gridPane.add(fantasyTeamComboBox,    1, 2, 1, 1);
        gridPane.add(rolePositionLabel,     0, 3, 1, 1);
        gridPane.add(rolePositionComboBox,  1, 3, 1, 1);
        gridPane.add(contractLabel,     0, 4, 1, 1);
        gridPane.add(contractComboBox,  1, 4, 1, 1);   
        gridPane.add(salaryLabel,     0, 5, 1, 1);
        gridPane.add(salaryTextField,  1, 5, 1, 1);
//        gridPane.add(posCCheckBox, 0, 4 , 1, 1);
//        gridPane.add(posOneBCheckBox, 1, 4 , 1, 1);
//        gridPane.add(posTwoBCheckBox, 2, 4 , 1, 1);
//        gridPane.add(posThreeBCheckBox, 3, 4 , 1, 1);
//        gridPane.add(posScCheckBox, 4, 4 , 1, 1);
//        gridPane.add(posOfCheckBox, 5, 4 , 1, 1);
//        gridPane.add(posPCheckBox, 6, 4 , 1, 1);
        
        gridPane.add(completeButton,    0, 6, 1, 1);
        gridPane.add(cancelButton,      1, 6, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
///////////////////////////////////////////////////////////////////////    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() { 
        return player;
    }
    
    public void addFantasyTeamName(String name) {
        fantasyTeamNames.add(name);
    }
    
    public ObservableList<String> setFantasyTeamNamesList(ObservableList<String> nameList) {
        fantasyTeamNames = nameList;
        return fantasyTeamNames;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public Player showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        player = new Player();
        
        // LOAD THE UI STUFF
        lastNameTextField.setText(player.getLastName());
        firstNameTextField.setText(player.getFirstName());
        proTeamComboBox.setValue(player.getProTeam());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return player;
    }
    
    public String getTeamSelection() {
        return (fantasyTeamComboBox.getSelectionModel().getSelectedItem().toString());
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        lastNameTextField.setText(player.getLastName());
        
        //int sessionsIndex = lecture.getSessions()-1;
        //sessionsComboBox.getSelectionModel().select(sessionsIndex);
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditPlayerDialog(Player playerToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        // LOAD THE LECTURE INTO OUR LOCAL OBJECT
        player.setProTeam(playerToEdit.getProTeam());
        player.setRolePosition(playerToEdit.getPosition());
        player.setContract(playerToEdit.getContract());
        player.setSalary(playerToEdit.getSalary());
        
//        player.setFirstName(playerToEdit.getFirstName());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    {
        
    }
}