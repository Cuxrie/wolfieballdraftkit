package csb.data;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * A simple data class for storing information about
 * an instructor.
 * 
 * @author Victor Cheng
 */
public class Player {
    StringProperty firstName, lastName, proTeam, position, notes, 
            birthYear, estimatedValue, birthNation, runsWins, rbiStrikeouts,
            homeRunsSaves, sbEarnedRunAverage, baWalksHitsInningsPitched,
            rolePosition, contract, fantasyTeam;
    double salary;
    
    
//    String initFirstName, String initLastName,String initProTeam,
//        String initPosition, String initNotes, String initBirthYear, 
//        String initEstimatedValue, String initBirthNation, String initWins,
//        String initStrikeouts, String initSaves,
//        String initEarnedRunAverage, String initWalksHitsInningsPitched
    public Player() {
        firstName = new SimpleStringProperty();
        lastName = new SimpleStringProperty();
        proTeam = new SimpleStringProperty();
        position = new SimpleStringProperty();
        notes = new SimpleStringProperty();
        birthYear = new SimpleStringProperty();
        estimatedValue = new SimpleStringProperty();
        birthNation = new SimpleStringProperty();
        runsWins = new SimpleStringProperty();
        rbiStrikeouts = new SimpleStringProperty();
        homeRunsSaves = new SimpleStringProperty();
        sbEarnedRunAverage = new SimpleStringProperty();
        baWalksHitsInningsPitched = new SimpleStringProperty();
        rolePosition = new SimpleStringProperty();
        contract = new SimpleStringProperty();
        salary = 0;
        fantasyTeam = new SimpleStringProperty();
    }

    public String getFirstName() 
    {
        return firstName.get();
    }
    
    public StringProperty firstNameProperty() 
    {
        return firstName;
    }

    public String getLastName() 
    {
        return lastName.get();
    }
    
    public StringProperty lastNameProperty() 
    {
        return lastName;
    }
    
    public String getProTeam() 
    {
        return proTeam.get();
    }

    public StringProperty proTeamProperty() 
    {
        return proTeam;
    }    
    
    public String getPosition() 
    {
        return position.get();
    }    
    
    public StringProperty positionProperty() 
    {
        return position;
    }
    
    public String getNotes() 
    {
        return notes.get();
    }    
    
    public String getBirthYear() 
    {
        return birthYear.get();
    }    
    
    public String getEstimatedValue() 
    {
        return estimatedValue.get();
    }    
    
    public String getBirthNation() 
    {
        return birthNation.get();
    }  
    
    public void setFirstName(String initFirstName) {
        firstName.set(initFirstName);
    }
    
    public void setLastName(String initLastName) {
        lastName.set(initLastName);
    }

    public void setProTeam(String initProTeam) {
        proTeam.set(initProTeam);
    }
    
    public void setPosition(String initPosition) {
    position.set(initPosition);
    }
    
    public void setNotes(String initNotes) {
    notes.set(initNotes);
    }
    
    public void setBirthYear(String initBirthYear) {
    birthYear.set(initBirthYear);
    }
    
    public void setEstimatedValue(String initEstimatedValue) {
    estimatedValue.set(initEstimatedValue);
    }
    
    public void setBirthNation(String initBirthNation) {
    birthNation.set(initBirthNation);
    }
    
        public String getRunsWins() 
    {
        return runsWins.get();
    }

    public StringProperty winsProperty() 
    {
        return runsWins;
    }        
        
    public String getRbiStrikeouts() 
    {
        return rbiStrikeouts.get();
    }
    
    public String getHomeRunsSaves()
    {
        return homeRunsSaves.get();
    }
    
    public String getSbEarnedRunAverage()
    {
        return sbEarnedRunAverage.get();
    }
    
    public String getBaWalksHitsInningsPitched()
    {
        return baWalksHitsInningsPitched.get();
    }
    
        public void setRunsWins(String initWins) 
    {
        runsWins.set(initWins);
    }

    public void setRbiStrikeouts(String initStrikeouts) 
    {
        rbiStrikeouts.set(initStrikeouts);
    }
    
    public void setHomeRunsSaves(String initSaves)
    {
        homeRunsSaves.set(initSaves);
    }
    
    public void setSbEarnedRunAverage(String initEarnedRunAverage)
    {
        sbEarnedRunAverage.set(initEarnedRunAverage);
    }
    
    public void setBaWalksHitsInningsPitched(String initWalksHitsInningsPitched)
    {
        baWalksHitsInningsPitched.set(initWalksHitsInningsPitched);
    }
    
    public String getRolePosition() 
    {
        return rolePosition.get();
    }   
    
    public String getContract() 
    {
        return contract.get();
    }
    
    public double getSalary() 
    {
        return salary;
    } 
    
    public void setRolePosition(String initRolePosition) 
    {
        rolePosition.set(initRolePosition);
    }   
    
    public void setContract(String initContract) 
    {
        contract.set(initContract);
    }
    
    public void setSalary(double initSalary) 
    {
        salary = initSalary;
    } 
    
    public String getFantasyTeam() 
    {
        return fantasyTeam.get();
    } 
    
    public void setFantasyTeam(String initFantasyTeam) 
    {
        fantasyTeam.set(initFantasyTeam);
    } 
//    public int compareTo(Object obj) {
//        Player otherPlayer = (Player)obj;
//        return getLastName().compareTo(otherPlayer.getLastName());
//    }
}
