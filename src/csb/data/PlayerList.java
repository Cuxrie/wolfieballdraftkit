package csb.data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class represents a course to be edited and then used to
 * generate a site.
 * 
 * @author Richard McKenna
 */
public class PlayerList {
    // THESE COURSE DETAILS DESCRIBE WHAT'S REQUIRED BY
    // THE COURSE SITE PAGES

    // THESE ARE THE THINGS WE'LL PUT IN OUR SCHEDULE PAGE
    ObservableList<Player> players;

    /**
     * Constructor for setting up a Course, it initializes the 
     * Instructor, which would have already been loaded from a file.
     * 
     * @param initInstructor The instructor for this course. Note that
     * this can be changed by getting the Instructor and then calling
     * mutator methods on it.
     */
    public PlayerList() {
        players = FXCollections.observableArrayList();
    }

    // BELOW ARE ALL THE ACCESSOR METHODS FOR A COURSE
    // AND THE MUTATOR METHODS. NOTE THAT WE'LL NEED TO CALL
    // THESE AS USERS INPUT VALUES IN THE GUI

    public void clearPlayers() {
        players.clear();
    }
    
    public void clearLectures() {
        players.clear();
    }
    
    public void addPlayer(Player player) {
        players.add(player);
    }
    
    public ObservableList<Player> getPlayers() {
        return players;
    }

    public void removeScheduleItem(Player playerToRemove) {
        players.remove(playerToRemove);
    }
}