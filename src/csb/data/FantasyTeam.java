package csb.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 *
 * @author VC
 */
public class FantasyTeam {
    
    String name;
    String owner;
    double money;
    
    ObservableList<Player> roster;
    ObservableList<Player> taxi;
    


    public FantasyTeam(){

        name = new String();
        owner = new String();
        money = 260;
        roster = FXCollections.observableArrayList();
        taxi = FXCollections.observableArrayList();

    }
    
    public String getName() {
        return name;
    }
    
    public String getOwner() {
        return name;
    }
    
    public double getMoney() {
        return money;
    }
    
    public void setName(String initName) {
        name = initName;
    }
    
    public void setOwner(String initOwner) {
        owner = initOwner;
    }
    
    public void setName(double initMoney) {
        money = initMoney;
    }
    
    public ObservableList<Player> getRoster() {
        return roster;
    }
    
    public ObservableList<Player> getTaxi() {
        return taxi;
    }

    public void setRoster(ObservableList<Player> initRoster) {
        roster = initRoster;
    }
    
    public void addRoster(ObservableList<Player> initRoster) {
        roster.addAll(initRoster);
    }
    
    public void setTaxi(ObservableList<Player> initTaxi) {
        taxi = initTaxi;
    }    
    
    public void addTaxi(ObservableList<Player> initTaxi) {
        taxi.addAll(initTaxi);
    }  
    
    public void addPlayer(Player pl) {
        roster.add(pl);
    }

    public void removePlayer(Player playerToRemove) {
        roster.remove(playerToRemove);
    }
}